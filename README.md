# goTerminal

像Cygwin的命令行go工具库 go Cygwin like terminal
go terminal for golang

## 用法

```go
package main

import (
	"fmt"
	"gitee.com/shirdonl/goTerminal"
	"os"
)

func main() {
	if goterminal.IsTerminal(os.Stdout.Fd()) {
		fmt.Println("Is Terminal")
	} else if goterminal.IsCygwinTerminal(os.Stdout.Fd()) {
		fmt.Println("Is Cygwin/MSYS2 Terminal")
	} else {
		fmt.Println("Is Not Terminal")
	}
}
```

## 安装

```
$ go get gitee.com/shirdonl/goTerminal
```

## License

MIT
